import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <div class="not-found">
      <h1>404</h1>
      <h2>Page not found</h2>
      <a routerLink="/">Go back</a>
    </div>
  `
})
export class NotFoundComponent {}
