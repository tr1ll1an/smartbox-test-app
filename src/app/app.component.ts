import { Component } from '@angular/core';
import { AuthService } from './shared/services/auth.service';
import { Observable } from 'rxjs';
import { UserProfile } from './shared/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  user$: Observable<UserProfile>;
  constructor(private authService: AuthService) {
    this.user$ = this.authService.currentUserObservable;
  }

  logout() {
    this.authService.logout();
  }
}
