// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyASkHwNm5aBHxsiG-0YZ80eE7RUvDxZ3qg',
    authDomain: 'smartbox-test.firebaseapp.com',
    databaseURL: 'https://smartbox-test.firebaseio.com',
    projectId: 'smartbox-test',
    storageBucket: 'smartbox-test.appspot.com',
    messagingSenderId: '266259916217',
    appId: '1:266259916217:web:f67622595243c171'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
