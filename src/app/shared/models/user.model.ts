export class UserProfile {
  age?: number;
  country?: string;
  email?: string;
  firstName?: string;
  lastName?: string;
  password?: string;
  confirmPassword?: string;
}

export class UserProfileFireStore {
  uid: string;
  age: number;
  country: string;
  firstName: string;
  lastName: string;
}
