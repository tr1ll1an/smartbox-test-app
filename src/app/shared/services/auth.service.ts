import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from 'firebase';
import { UserService } from './user.service';
import { UserProfile } from './../models/user.model';

import { switchMap, startWith, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  USER: any = null;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private userService: UserService,
    private afs: AngularFirestore
  ) {
    // this.realTimeLocalStorage();
  }

  // Realtime LocalStorage change
  private realTimeLocalStorage() {
    this.afAuth.authState
      .pipe(
        switchMap(user => {
          return user
            ? this.afs.doc<User>(`users/${user.uid}`).valueChanges()
            : of(null);
        }),
        tap(user => localStorage.setItem('user', JSON.stringify(user))),
        startWith(JSON.parse(localStorage.getItem('user')))
      )
      .subscribe(user => (this.USER = user));
  }

  get currentUserObservable(): any {
    return this.afAuth.authState;
  }

  async register(user) {
    await this.afAuth.auth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(data => {
        const uid = data.user.uid;
        const { firstName, lastName, age, country } = user;

        this.userService
          .createUser(uid, { firstName, lastName, age, country })
          .then(() => this.router.navigate(['profile']));
      });
  }

  async login(email: string, password: string) {
    await this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.router.navigate(['profile']);
      });
  }

  async logout() {
    await this.afAuth.auth.signOut();
    localStorage.removeItem('user');
    this.router.navigate(['login']);
  }
}
