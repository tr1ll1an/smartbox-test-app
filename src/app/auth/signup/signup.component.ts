import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClrForm, ClrLoadingState } from '@clr/angular';
import { UserProfile } from 'src/app/shared/models/user.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html'
})
export class SignupComponent implements OnInit {
  @ViewChild(ClrForm) clrForm: ClrForm;
  submitBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  registerForm: FormGroup;
  submitted = false;
  registerError: string;
  hasError = false;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  get email() {
    return this.registerForm.get('email');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }
  get age() {
    return this.registerForm.get('age');
  }

  initForm() {
    this.registerForm = this.formBuilder.group(
      {
        firstName: [''],
        lastName: [''],
        email: [
          '',
          [
            Validators.required,
            Validators.pattern(
              '[a-zA-Z0-9._-]{1,}@[a-zA-Z0-9_]{1,}[.]{1}[a-zA-Z]{2,}'
            )
          ]
        ],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirmPassword: ['', Validators.required],
        age: ['', [Validators.pattern(/^[0-9]*$/)]],
        country: ['']
      },
      {
        validator: [this.MustMatch('password', 'confirmPassword')]
      }
    );
  }

  onSubmit() {
    this.submitBtnState = ClrLoadingState.LOADING;
    this.hasError = false;
    this.submitted = true;

    if (this.registerForm.invalid) {
      this.clrForm.markAsDirty();
      this.submitBtnState = ClrLoadingState.DEFAULT;
      return;
    }

    const user = this.trimValue(this.registerForm.value);
    this.authService
      .register(user)
      .catch(err => {
        this.registerError = err.message;
        this.hasError = true;
      })
      .finally(() => {
        this.submitted = false;
        this.submitBtnState = ClrLoadingState.DEFAULT;
      });
  }

  private trimValue(userForm: UserProfile) {
    const user = {};
    Object.keys(userForm).map(k => (user[k] = userForm[k].trim()));
    return user;
  }

  private MustMatch(password: string, confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
