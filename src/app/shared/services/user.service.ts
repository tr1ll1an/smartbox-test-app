import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserProfile, UserProfileFireStore } from './../models/user.model';
import { Observable, of } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { switchMap } from 'rxjs/operators';
import { User } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {}

  public createUser(uid: string, user: UserProfile) {
    return this.db
      .doc(`users/${uid}`)
      .set({ ...user, uid } as UserProfileFireStore);
  }

  public getUser(): Observable<any> {
    return this.afAuth.authState.pipe(
      switchMap(userState => {
        return userState
          ? this.afs.doc<User>(`users/${userState.uid}`).valueChanges()
          : of(null);
      })
    );
  }

  public updateUser(user: UserProfileFireStore): Observable<any> {
    return this.afAuth.authState.pipe(
      switchMap(userState => {
        return this.afs.doc<User>(`users/${userState.uid}`).update(user);
      })
    );
  }
}
