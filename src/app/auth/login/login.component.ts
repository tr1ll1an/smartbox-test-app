import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { ClrForm } from '@clr/angular';
import { ClrLoadingState } from '@clr/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  @ViewChild(ClrForm) clrForm: ClrForm;

  submitBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  loginForm: FormGroup;
  submitted = false;
  loginError = false;
  loginPassError = 'Invalid user name or password';

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  get email() {
    return this.loginForm.get('email');
  }

  get password() {
    return this.loginForm.get('password');
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitBtnState = ClrLoadingState.LOADING;
    this.loginError = false;
    this.submitted = true;
    if (this.loginForm.invalid) {
      this.clrForm.markAsDirty();
      this.submitBtnState = ClrLoadingState.DEFAULT;
      return;
    }
    const { email, password } = this.loginForm.controls;

    this.authService
      .login(email.value, password.value)
      .catch(() => (this.loginError = true))
      .finally(() => {
        this.submitted = false;
        this.submitBtnState = ClrLoadingState.DEFAULT;
      });
  }
}
