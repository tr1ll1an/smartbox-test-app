## How to start
- `npm install` - install packages
- `ng serve` or `npm run start` - to start the server
- Navigate to `http://localhost:4200/`.

## References:

- [x] [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9
- [x] [Clarity Design System](https://v1.clarity.design/) **UI components** version 1.1.3
- [x] [Firebase](https://firebase.google.com) Auth,DB


## Demo

 http://smartbox.trillian.pro