import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { UserService } from '../shared/services/user.service';
import { UserProfileFireStore, UserProfile } from '../shared/models/user.model';
import { LabelService } from '../shared/services/label.sevice';
import { LabelProfile } from '../shared/models/labels.model';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ClrLoadingState } from '@clr/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('firstName') firstNameElement: ElementRef;

  successUpdate = false;
  disabled = true;
  user: UserProfileFireStore | any;
  labels: LabelProfile;
  currentUser: UserProfileFireStore;
  validateBtnState: ClrLoadingState = ClrLoadingState.DEFAULT;
  editUserForm: FormGroup;
  editError = false;

  constructor(
    private userService: UserService,
    private labelService: LabelService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
    this.userService.getUser().subscribe(data => {
      if (data) {
        this.user = data;
        this.editUserForm.patchValue(data);
      }
    });

    this.labelService.getLabels().subscribe(data => {
      this.labels = data;
    });
  }
  get fullName() {
    return `${this.user.firstName} ${this.user.lastName}`;
  }
  get age() {
    return this.editUserForm.get('age');
  }

  initForm() {
    this.editUserForm = this.formBuilder.group({
      firstName: [''],
      lastName: [''],
      age: ['', Validators.pattern(/^[0-9]*$/)],
      country: ['']
    });
  }

  rejectEdit() {
    this.editUserForm.patchValue(this.user);
    this.disabled = true;
  }

  onSubmit() {
    this.validateBtnState = ClrLoadingState.LOADING;
    if (this.editUserForm.invalid) {
      this.validateBtnState = ClrLoadingState.DEFAULT;
      return;
    }

    this.userService.updateUser(this.editUserForm.value).subscribe(
      data => {
        this.disabled = true;
        this.successUpdate = true;
        this.validateBtnState = ClrLoadingState.DEFAULT;
        setTimeout(() => (this.successUpdate = false), 2500);
      },
      err => {
        this.editError = true;
        this.validateBtnState = ClrLoadingState.DEFAULT;
      }
    );
  }

  editForm() {
    this.disabled = !this.disabled;
    this.firstNameElement.nativeElement.focus();
  }
}
