import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LabelService {
  constructor(private db: AngularFirestore) {}

  public getLabels(): Observable<any> {
    return this.db.doc(`pages/profile`).valueChanges();
  }
}
